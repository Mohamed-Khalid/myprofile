import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
   CustomTextField({
    Key? key,
    required this.labelName,
     this.prefixIcon,
     this.controller,
     this.hintText
  }) : super(key: key);

  final String labelName ;
  final Icon? prefixIcon ;
 final TextEditingController? controller ;
 final String? hintText ;
  bool isVis = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 20
      ),
      child: TextField(
        controller: controller ,
        decoration: InputDecoration(
          label: Text(
            labelName,
            style: const TextStyle(
                fontSize: 18,
                color: Colors.blue
            ),
          ),
          hintText: hintText,
          prefixIcon:prefixIcon,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: const BorderSide(color: Colors.black, width: 2)
          ),
        ),
      ),
    );
  }
}
