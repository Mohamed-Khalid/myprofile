import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const  HomeScreen({Key? key, required this.userId}) : super(key: key);
  final String userId ;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerScrimColor: Colors.tealAccent.withOpacity(.3),
      backgroundColor: Colors.tealAccent,
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title:const Text(
          "Home",
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25
          ),
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.teal,
        width: 300,
        child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
          future: FirebaseFirestore.instance.collection("users").doc(userId).get(),
            builder:(context, snapshot) {
              switch(snapshot.connectionState){
                case ConnectionState.waiting :
                  return const CircularProgressIndicator();
                case ConnectionState.none:
                  return const CircularProgressIndicator();
                case ConnectionState.active :
                  return const CircularProgressIndicator();
                case ConnectionState.done :
                  if(snapshot.hasData){
                    return Column(
                      children: [
                        UserAccountsDrawerHeader(
                          decoration: const BoxDecoration(
                              color: Colors.blueGrey,
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30),
                                bottomRight: Radius.circular(30),
                              )
                          ) ,
                          accountName: Text("Hi ${snapshot.data?["userName"]} "),
                          accountEmail: Text(" ${snapshot.data?["userEmail"]}"),
                          currentAccountPicture: ClipRRect(
                            borderRadius: BorderRadius.circular(60),
                            child: Image.network("${snapshot.data?["userImage"]}"),

                          ),
                        ),
                      ],
                    );
                  }
              }
               return const Center(child: Text("Error!",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 50
                ),
              ),
              );
            },
        )
      ),
    );
  }
}

