import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:my_test_app/screens/home_screen.dart';
import 'package:my_test_app/shared/custom_text_field.dart';

class SignUpScreen extends StatelessWidget {
   SignUpScreen({Key? key}) : super(key: key);
 final TextEditingController name = TextEditingController();
 final TextEditingController email = TextEditingController();
 final TextEditingController pass = TextEditingController();
 final TextEditingController phone = TextEditingController();
 final TextEditingController address = TextEditingController();
 final TextEditingController image = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.tealAccent,
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title:const Text(
            "Sign Up",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25
          ),
        ),
        actions: [
          IconButton(
            padding: const EdgeInsets.only(
              right: 20,
            ),
              onPressed: (){},
              icon:const Icon(
                Icons.logout,
                size: 42,
              ),
          )
        ],
      ),
      body: ListView(
        children: [
          CustomTextField(
            controller: name,
            labelName: "User Name",
            prefixIcon:const Icon(
              Icons.person,
              size: 42,
              color: Colors.black,
            ) ,
          ),
          CustomTextField(
            controller: email,
            labelName: "User Email",
            prefixIcon:const Icon(
              Icons.email_outlined,
              size: 42,
              color: Colors.black,
            ) ,
          ),
          CustomTextField(
            controller: pass,
            labelName: "User Password",
            prefixIcon:const Icon(
              Icons.lock,
              size: 42,
              color: Colors.black,
            ) ,
          ),
          CustomTextField(
            controller: phone,
            labelName: "User Phone",
            prefixIcon:const Icon(
              Icons.phone,
              size: 42,
              color: Colors.black,
            ) ,
          ),
          CustomTextField(
            controller: address,
            labelName: "User Location",
            prefixIcon:const Icon(
              Icons.location_city,
              size: 42,
              color: Colors.black,
            ) ,
          ),
          CustomTextField(
            hintText: "Please Enter The Image URl",
            controller: image,
            labelName: "User Image",
            prefixIcon:const Icon(
              Icons.image,
              size: 42,
              color: Colors.black,
            ) ,
          ),
          InkWell(
            onTap: () async{
              await signUp(
                email.text,
                pass.text,
                context
              );

            },
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(
                vertical: 15,
                horizontal: 40
              ),
              padding: EdgeInsets.symmetric(
                  vertical: 10,
              ),
              decoration: BoxDecoration(
                color: Colors.teal,
                borderRadius: BorderRadius.circular(30)
              ),
              child: Text(
                "Sign Up",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
 Future<void> signUp(String userEmail , String userPassword , BuildContext context) async{
   await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: userEmail,
        password: userPassword,
    ).then((value){
        CollectionReference userRef =  FirebaseFirestore.instance.collection("users");
        String? id = value.user?.uid;
        userRef.doc(value.user?.uid).set({
          "userId" : value.user?.uid,
          "userName" : name.text,
          "userEmail" : email.text,
          "userPass" : pass.text,
          "userPhone" : phone.text,
          "userAdd" : address.text,
          "userImage" : image.text,
        }
        ).then((value){
          Navigator.pushReplacement(context,
              MaterialPageRoute(
                builder: (context) => HomeScreen(
                  userId: id!,
                ),
              )
          );
        });
            },onError: (e){
      print("Erororoonffffffffffffffffff ${e.toString()}");
    }
            );
  }
}
